=== Toy Sitemap ===
Contributors: thewhodidthis
Tags: google, seo
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Creates a basic XML sitemap for WP posts and pages.
